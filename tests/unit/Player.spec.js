import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import Player from '@/components/Player.vue'
import loading from '@/store/modules/loading'

const localVue = createLocalVue()
localVue.use(Vuex)

describe('Player.vue', () => {
  let store
  let data = ''

  beforeEach(() => {
    store = new Vuex.Store({
      modules: {
        file: {
          state: { data }
        },
        loading
      }
    })
  })

  it('renders expected output', () => {
    const wrapper = shallowMount(Player, { store, localVue })
    expect(wrapper.element).toMatchSnapshot()
  })
  it('renders loading message when data is not available', () => {
    data = ''
    const wrapper = shallowMount(Player, { store, localVue })
    const message = wrapper.find('.placeholder')
    expect(message.text()).toEqual('Aguardando dados da Partitura')
  })
  it('does not render tanana-player when data is not available', () => {
    data = ''
    const wrapper = shallowMount(Player, { store, localVue })
    const player = wrapper.find('tanana-player')
    expect(player.exists()).not.toBe(true)
  })
  // skipping tests that are crashing due to jsdom/jest/vue/babel configs
  it.skip('renders tanana-player web component when data is available', () => {
    data = 'some valid xml'
    const wrapper = shallowMount(Player, { store, localVue })
    const player = wrapper.find('tanana-player')
    expect(player.exists()).toBe(true)
  })
  it.skip('does not render loading message when data is available', () => {
    data = 'some valid xml'
    const wrapper = shallowMount(Player, { store, localVue })
    const message = wrapper.find('.placeholder')
    expect(message.exists()).not.toBe(true)
  })
})
