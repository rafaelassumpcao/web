import { shallowMount } from '@vue/test-utils'
import XMLSelector from '@/components/XMLSelector.vue'

describe('XMLSelector.vue', () => {
  it('renders expected output', () => {
    const wrapper = shallowMount(XMLSelector)
    expect(wrapper.element).toMatchSnapshot()
  })
})
