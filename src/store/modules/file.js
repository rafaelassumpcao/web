const state = {
  data: ''
}

const getters = {
}

const actions = {
  updateFile ({ commit, dispatch }, { file }) {
    dispatch('loading/start', null, {
      root: true
    })
    const reader = new FileReader()
    reader.onload = (event) => {
      const data = event.target.result
      commit('setData', data)
      dispatch('loading/finish', null, {
        root: true
      })
    }
    reader.readAsText(file)
  },
  updateFileContent ({ commit, dispatch }, { xmlData }) {
    commit('setData', xmlData)
    dispatch('loading/finish', null, {
      root: true
    })
  }
}
const mutations = {
  setData (state, data) {
    state.data = data
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
